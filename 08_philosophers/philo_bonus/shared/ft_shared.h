#ifndef SHARED_H
# define SHARED_H

# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <fcntl.h>
# include <limits.h>

/* ./ft_atol.c ============================================================== */
long	ft_atol(const char *nptr);

/* ./ft_isdigit.c =========================================================== */
int		ft_isdigit(int c);
#endif
