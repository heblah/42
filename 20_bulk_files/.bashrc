# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    .zshrc                                             :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: halvarez <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/03/21 19:45:48 by halvarez          #+#    #+#              #
#    Updated: 2022/08/11 16:34:36 by halvarez         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

alias ga='git add --all -v'
alias gc='git commit -am $1'
alias gac='git add --all -v && git commit -am $1'
alias gp='git push'
